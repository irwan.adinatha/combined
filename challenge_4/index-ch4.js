class game{
  constructor(winScore,gameMode){
    this.gameMode = gameMode;
    this.winscore = winScore;
    this.result = "";
  }

  gameRule(p1Select, p2Select){
    const p1SelectArray = p1Select.split("_");
    p1Select = p1SelectArray[1];
    const p2SelectArray = p2Select.split("_");
    p2Select = p2SelectArray[1];

    if(p1Select == p2Select){
      return "Draw";
    }else if(p1Select=="gunting" && p2Select=="batu"){
      this.player2Score++;
      return "p2 Win";
    }else if(p1Select=="gunting" &&p2Select=="kertas"){
      this.player1Score++;
      return "p1 Win";
    }else if(p1Select=="batu" && p2Select=="gunting"){
      this.player1Score++;
      return "p1 Win";
    }else if(p1Select=="batu" && p2Select=="kertas"){
      this.player2Score++;
      return "p2 Win";
    }else if(p1Select=="kertas" && p2Select=="gunting"){
      this.player2Score++;
      return "p2 Win";
    }else if(p1Select=="kertas" && p2Select=="batu"){
      this.player1Score++;
      return "p1 Win";
    }
  }

  gameStart(){ 
    document.getElementById("vs-win-lose-div").className = "vs";
    document.getElementById("vs-win-lose-div").innerHTML = "VS";
  }


}

class player {
  constructor(playerName,playerType,score,selected){
    this.name = playerName;
    this.playerType = playerType;
    this.score = 0;
    this.selected = null;
  }

  
  win(){
    this.score++;
    document.getElementById("vs-win-lose-div").className = "win";
    document.getElementById("vs-win-lose-div").innerHTML = this.name + "<BR>WIN";
  }

  draw(){
    document.getElementById("vs-win-lose-div").className = "draw";
    document.getElementById("vs-win-lose-div").innerHTML = "DRAW";
  }

}


class comPlayer extends player{
  constructor(playerName,playerType,score,selected){
    super(playerName, playerType, score, selected)
  }

  rand(min, max){
    return (Math.floor(Math.pow(10,14)*Math.random()*Math.random())%(max-min+1))+min;
  }

  assignComSelection(){
    let comSelection = this.rand(1,3);
    //console.log("comSelection is " + comSelection);

    // let comSelection = Math.floor(Math.random() * 3) + 1;
    switch(comSelection){
      case 1:
        return "player2_gunting";
        break;
      case 2:
        return "player2_batu";
        break;
      case 3:
        return "player2_kertas";
        break;
    }
  }

}

let numberOfGames = 5; // add a choice for user to select number of games

//insert selector here for PvC or PvP option later
let gameRunning = new game(numberOfGames, "PvC");
//console.log(gameRunning);
//star a new game
gameRunning.gameStart();
startEventListener(gameRunning.gameMode, "start");

let P1= new player("Irwan","Human");
//give conditional here later based on selected mode, in the class
let P2= new comPlayer("COM","Com");


function handleClick(){
  startEventListener(gameRunning.gameMode, "kill");
  P1.selected = this.id;
  console.log(P1.selected);
  P2.selected = P2.assignComSelection();
  console.log(P2.selected);
  let hasil  =  gameRunning.gameRule(P1.selected, P2.selected);
  console.log(hasil);
  if(hasil == "p1 Win"){
    P1.win();
  }else if(hasil == "p2 Win"){
    P2.win();
  }else if(hasil == "Draw"){
    P1.draw();
  }
   //set the display to show selection on the page
   document.getElementById(this.id).style.background = "grey";
   document.getElementById(P2.selected).style.background = "grey";
  // gameRunning.player1Selection = this.id;
  // gameRunning.run();
    updateScore(); //update the score

    
    checkGameOver(); //check if it is gameover

}



function startEventListener(mode, action){
  if(action=="start"){
    document.getElementById("ulang").addEventListener("click", ulangClick);
    document.getElementById("ulang").addEventListener("mouseover", animateSelection);
    document.getElementById("ulang").addEventListener("mouseout", animateSelectionReset);
    if(mode == "PvC" || mode == "PvP"){
      let numberOfGambarButtons1 = document.querySelectorAll(".gambar1").length;
      //watch event for play button
      for(var i=0 ; i<numberOfGambarButtons1 ; i++) {
        document.querySelectorAll(".gambar1")[i].addEventListener("click", handleClick);
        document.querySelectorAll(".gambar1")[i].addEventListener("mouseover", animateSelection);
        document.querySelectorAll(".gambar1")[i].addEventListener("mouseout", animateSelectionReset);
      }
    }
    if(mode == "PvP"){
      let numberOfGambarButtons2 = document.querySelectorAll(".gambar2").length;
      for(var i=0 ; i<numberOfGambarButtons2 ; i++) {
        document.querySelectorAll(".gambar2")[i].addEventListener("click", handleClick);
        document.querySelectorAll(".gambar2")[i].addEventListener("mouseover", animateSelection); 
        document.querySelectorAll(".gambar2")[i].addEventListener("mouseout", animateSelectionReset);
      }
    }

  

  }else{
    //kill event listener
    let numberOfGambarButtons1 = document.querySelectorAll(".gambar1").length;
    for(var i=0 ; i<numberOfGambarButtons1 ; i++) {
      document.querySelectorAll(".gambar1")[i].removeEventListener("click", handleClick);
      document.querySelectorAll(".gambar1")[i].removeEventListener("mouseover", animateSelection);
      document.querySelectorAll(".gambar1")[i].removeEventListener("mouseout", animateSelectionReset);
    }
  
    let numberOfGambarButtons2 = document.querySelectorAll(".gambar2").length;
    for(var i=0 ; i<numberOfGambarButtons2 ; i++) {
      document.querySelectorAll(".gambar2")[i].removeEventListener("click", handleClick);
      document.querySelectorAll(".gambar2")[i].removeEventListener("mouseover", animateSelection);
      document.querySelectorAll(".gambar2")[i].removeEventListener("mouseout", animateSelectionReset);
    }
  }

}
function animateSelection(){
  // console.log("animate");
  document.getElementById(this.id).style.transform = "rotate(7deg)";
}

function animateSelectionReset(){
  // console.log("animateReturn");
  document.getElementById(this.id).style.transform = "rotate(-7deg)";
}
function ulangClick(){
  //reset for next game;
  console.log("ulang");
  //clear all the selected styles;
  let numberOfGambarButtons1 = document.querySelectorAll(".gambar1").length;
  for(var i=0 ; i<numberOfGambarButtons1 ; i++) {
    document.querySelectorAll(".gambar1")[i].style.background= "";
    document.querySelectorAll(".gambar1")[i].style.transform= "none";
  }
  let numberOfGambarButtons2 = document.querySelectorAll(".gambar2").length;
  for(var i=0 ; i<numberOfGambarButtons2 ; i++) {
    document.querySelectorAll(".gambar2")[i].style.background= "";
    document.querySelectorAll(".gambar2")[i].style.transform= "none";

  }
  //reset selected icon
  //reset addEventListener according to gameMode
  startEventListener(gameRunning.gameMode, "start");
  gameRunning.gameStart();
}


function updateScore(mode){
  if(mode== "reset"){
    document.getElementById("p1score").innerHTML = "0";
    document.getElementById("p2score").innerHTML = "0";
    //reinit game object
    P1.score = 0;
    P2.score = 0;
  }else{
    document.getElementById("p1score").innerHTML = P1.score;
    document.getElementById("p2score").innerHTML = P2.score;
    console.log("Player 1 Score " + P1.score + " ; " + "Player 2 Score " + P2.score + " ; ");
  }
}


function checkGameOver(){
  //set the game winning limit
  if(P1.score == gameRunning.winscore){
    alert(P1.name + " win");
    updateScore("reset");
    console.clear();
    ulangClick();
  }else if (P2.score == gameRunning.winscore){
    alert(P2.name + " win");
    updateScore("reset");
    console.clear();
    ulangClick();
  }
  //lock everything and set everything to fresh start

}






