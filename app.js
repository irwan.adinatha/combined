const express= require("express");
const https=require("https");
const bodyParser = require("body-parser");
const exp = require("constants");
const app = express();
const port = 3000;

app.use(express.static("Chapter3_Wave20"));
app.use(express.static("challenge_4"));

const logger = (req, res, next) => {
    console.log (req.method +" "+ req.url);
    next();
}

app.use(logger);

const router_ch4 = require('./challenge_4');
app.use(router_ch4);




app.use(bodyParser.urlencoded({extended:true}));

app.get("/", function(req,res){
    console.log("running.");
    res.sendFile(__dirname + "/Chapter3_Wave20/index-ch3.html");
})

app.post("/rockpaperstrategy", function(req,res){
    console.log("roc paper stone choosen");
    //redirect to rock paper game
    res.sendFile(__dirname  + "/challenge_4/index-ch4.html");
})




app.listen(port, function(){
    console.log("Server Running on port 3000.")
})

